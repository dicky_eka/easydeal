<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






Auth::routes();

Route::get('/', 'PageController@home');
Route::get('/project', 'PageController@project');
Route::get('/agent', 'PageController@agent');
Route::get('/expo', 'PageController@expo');
Route::get('/contact', 'PageController@contact');
Route::get('/brochure', 'PageController@brochure');
Route::get('post', 'PostController@index');
Route::get('post/{slug}', 'PostController@show')->name('post.show');
Route::get('program-info', 'PageController@programInfo');

Route::get('admin/home', 'HomeController@index');
Route::group(['middleware'=>'auth','namespace'=>'Admin','prefix' => 'admin','as' => 'admin.'],function (){
    Route::resource('post', 'PostController');
    Route::resource('agent', 'AgentController');    
    Route::resource('location', 'AgentLocationController');
    Route::resource('brochure', 'BrochureController');
    Route::resource('contact', 'ContactController');
    Route::resource('expo', 'ExpoController');    
    Route::resource('project', 'ProjectController');
    Route::resource('user', 'UserController');    
    Route::resource('about', 'AboutController');
    Route::resource('logo', 'LogoController');
    Route::resource('gift', 'GiftController');
    Route::resource('block', 'BlockController');
    Route::get('program-info','PageController@programInfo');
});