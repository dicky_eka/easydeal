@extends('layouts.web')

@section('content')
<section style="padding-top:50px;padding-bottom:50px">
    <div class="container content" style="text-align: center">
        <div class="row">
            <div class="col-md-12" style="text-align: center">
                <h1 style="margin-bottom:15px; color:#ed1c24">EXPO & EVENTS</h1>
            </div>
        </div>
    </div>
</section>
<section style="padding-top:50px;padding-bottom:30px">
    <div class="container content" style="text-align: center"><!-- 1 -->
        <div class="row"><!-- 2 -->

            @foreach($expos as $expo)
            <div class="col-md-6" style="text-align: center; padding-bottom: 30px" ><!-- 3 -->
                <h3 style="width:100%;background-color:#ed1c24; padding-top:20px;padding-bottom:20px;margin-bottom: 0; color:#ffffff">{{$expo->on_date}}</h3>
                <p style="border-right: 1px solid gray; padding-top:20px;padding-bottom:20px;border-left: 1px solid gray; border-bottom:1px solid gray"> {{$expo->location}}</p>
            </div><!-- 3 -->
            @endforeach
        </div><!-- 2 -->
    </div><!-- 1 -->
</section>
@stop