@extends('layouts.web')

@section('content')
<div class="container content" style="padding-top: 30px">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <section style="padding-top:0;padding-bottom:0; margin-bottom:0;">
                <!-- AGENCY ITEM -->
                @foreach($contacts as $contact)
                    <div class="agency-item">
                        <div class="left-a">
                            <a href="http://www.bsdcity.com/site/" target="_blank"></a>
                            <img src="{{asset('images/project-logo/1-bsd.png')}}">
                            </a>
                        </div>
                        <div class="center-a">
                            <div class="agency-item-inner">
                                {{$contact->address}}
                            </div>
                        </div>
                        <div class="right-a">
                                <a class="link-animate" href="tel:2153159000">{{$contact->phone}}</a>
                        </div>
                    </div>
                @endforeach
            </section>


        </div>
    </div>
</div>

<section style="padding-top:50px;padding-bottom:50px">
    <div class="container content" style="text-align: center">
        <div class="row">
            <div class="col-md-12" style="text-align: center">
                <h2 style="margin-bottom:15px; color:#ed1c24">BOOK YOUR UNIT NOW</h2>
                <h6>Hanya agen kantor terdaftar yang bisa booking unit</h6>
                <h1 style="color:#ed1c24">1500 238</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: center">
                <a href="tel:1500238"><h6 style="color:#ffffff; background-color:#ed1c24; padding:10px;width:100%;text-align: center">CONTACT US</h6></a>
            </div>
        </div>

    </div>
</section>
@stop