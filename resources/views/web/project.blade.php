@extends('layouts.web')

@section('content')
<section style="padding-top:0px; padding-bottom:10%">
    <div class="container">
        <div class="intro">
            <h1 style="color:#ed1c24; text-align:center;">PROJECTS</h1>
        </div>
        <div class="row" style="padding-bottom: 2%">
            @foreach($projects as $project)
                <div class="col-md-4" style="padding-bottom: 4%">
                    <a class="project-link" href="{{$project->url}}"><img src="{{asset($project->image)}}" width="100%"></a>
                </div>
            @endforeach
        </div>
    </div>
    </div>
</section>
@stop