@extends('layouts.web')

@section('content')
<section style="padding-top:50px; padding-bottom: 0px; ">
    <div class="container content"><!-- 1 -->

        <h1 style="color:#ed1c24; text-align:center; padding-top:0;padding-bottom: 5%">ASSIGNED AGENTS</h1>
        @foreach($agentLocation as $location)
        <!-- panel 1 -->
        <div class="row"> <!-- 2 -->
            <div class="col-md-12"><!-- 3 -->
                <button class="accordion button-text">{{$location->name}}</button> <!-- button open and close -->
                <div class="panel"><!-- 4 -->


                <!-- panel 1 row 1 -->
                    <div class="row" ><!-- 5 -->
                        @foreach($location->agents as $agent)

                        <div class="col-lg-6" style="padding-top: 3%"><!-- 6 -->
                            <h4>{{$agent->name}}</h4>
                            <p>ADDRESS: <span style="color:#ed1c24">{{$agent->address}}</span><br>
                                PHONE: <span style="color:#ed1c24">{{$agent->phone}}</span><br>
                                EMAIL: <span style="color:#ed1c24">{{$agent->email}}</span></p>
                        </div><!-- 6 -->
                        @endforeach

                    </div><!-- 5 --> <!-- panel 1 row 1 ends -->

                </div><!-- 4 -->
            </div><!-- 3 -->
        </div><!-- 2 -->

        @endforeach
    </div> <!-- 1 -->
</section><!-- panel 1 ends-->
@stop

@section('js')
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active1");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
</script>

@stop