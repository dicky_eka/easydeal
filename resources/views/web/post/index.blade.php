@extends('layouts.web')

@section('content')
<section style=" padding-top:0; padding-bottom: 10px">
    <div class="container intro">
        <div class="row">
            @foreach($posts as $post)
            <div class="col-md-4">
                <ul class="posts">
                    <li class="post item">
                        <div class="date">{{$post->published_at->format('F-d-Y')}}</div>
                        <a href="{{route('post.show',$post->slug)}}">
                            <h4 style="padding-bottom:50px">{{str_limit($post->title,50)}}</h4>
                            <img alt="" src="{{asset($post->image)}}" width="100%;" style="padding-bottom:40px">
                            <p>{!! str_limit(strip_tags($post->content),100) !!}</p>
                        </a>
                    </li>
                </ul>
            </div>
            @endforeach
        </div>
    </div>
</section>

@stop