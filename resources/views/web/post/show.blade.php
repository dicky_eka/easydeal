@extends('layouts.web')
@section('meta')
    <meta name="description" content="{{ empty($post->meta_description) ? str_limit($post->content) : strip_tags($post->meta_description) }}"/>
    <meta name="keywords" content="{{$post->meta_keyword}}"/>
    <meta property="og:title" content="{{$post->title}}" />
    <meta property="og:url" content="{{route('post.show',$post->slug)}}" />
    <meta property="og:site_name" content="Easydeal" />
    <meta name="author" content="{{$post->user->name}}"><meta content='IND' name='language'/>
    <meta content='id' name='geo.country'/>
    <meta content='Indonesia' name='geo.placename'/>
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
@stop
@section('content')
    <section style="padding-top: 50px;">
        <div class="container intro">
            <div class="row">
                <div class="col-md-2">
                    <div class="date">{{$post->published_at->format('F-d-Y')}}</div>
                </div>
                <div class="col-md-8">
                    <div class="info">
                        <div class="tags">{{$post->tags}}</div>
                        <div class="author">by {{$post->user->name}}</div>
                    </div>
                    <div class="content">
                        <h1>{{$post->title}}</h1>
                        <img alt="" src="{{asset($post->image)}}" width="100%">
                        <div class="description">
                            {!! $post->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@stop