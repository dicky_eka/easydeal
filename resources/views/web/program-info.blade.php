@extends('layouts.web')

@section('content')
<section style="padding-top:30px; padding-bottom: 50px; ">
        <div class="container content">
            <div class="row">
                <div class="col-md-12">
                    {!! $block->firstWhere('label', 'heading_1')->content !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <img class="program-1" src="{{ asset($block->firstWhere('label', 'banner')->content) }}" width="100%">
                </div>

            </div>
    </section>


<section style="padding-top:30px; padding-bottom: 10% ; ">
        <div class="container content">
            <div class="row">
                <div class="col-md-12">
                    {!! $block->firstWhere('label', 'heading_2')->content !!}
                    <hr style="border:0.3px solid gray">
                </div>
            </div>
            <div class="row">
            @foreach($gift as $g)
                @if($loop->index == 0)
                    <div class="row" style="padding-bottom:30px">
                        <div class="col-md-6">
                            <img src="{{asset($g->image)}}" width="100%">
                        </div>

                        <div class="col-md-6">
                            <h3 style="color:#ed1c24; text-align: center; padding-top:20%;padding-bottom: 4%;">{{$g->title}}</h3>
                            {!! $g->description !!}
                        </div>
                    </div>
                @else
                    <div class="col-md-6">
                        <img src="{{asset($g->image)}}" width="100%">
                        <h3 style="color:#ed1c24; text-align: center;">{{$g->title}}</h3>
                    </div>
                @endif

            @endforeach
            </div>



            <div class="row">
                <div class="col-md-12">
                    <hr style="border:0.3px solid gray">
                    <p>

                    @foreach($gift as $g)
                        @if($loop->index != 0)
                       {{strip_tags($g->description)}}<br>
                            @endif
                    @endforeach
                    </p>
                </div>


            </div>


    </section>

<section style="padding-top:0;padding-bottom:0; margin-bottom:0;background-color: #f0f0f0"">
    <div class="container content" style="padding-top: 30px">
        <div class="row">
            <div class="col-md-12">
                {!! $block->firstWhere('label', 'heading_3')->content !!}
            </div>
        </div>

        <div data-visible="4" auto-slide class="slider carousel">
            <ul class="slides">
                @foreach($logoProject as $logo)
                    <li class="slide text-center item vcenter"><img alt="" src="{{asset($logo->image)}}" class="thumb"></li>
                @endforeach
            </ul>
        </div>
    </div>
</section>

<section style="padding-top:50px;padding-bottom:50px">
    <div class="container content" style="text-align: center">
        <div class="row">
            <div class="col-md-12" style="text-align: center">
                {!! $block->firstWhere('label', 'heading_4')->content !!}
                <h1 style="color:#ed1c24">{{$about->phone}}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: center">
                <a href="tel:{{$about->phone}}"><h6 style="color:#ffffff; background-color:#ed1c24; padding:10px;width:100%;text-align: center">CONTACT US</h6></a>
            </div>
        </div>

    </div>
</section>

<section style="padding-top:10px;padding-bottom: 30px">
    <img src="{{asset('images/program-info/divider.jpg')}}" width="100%" style="padding:0">
</section>

<section style="padding-top:10px;padding-bottom: 7%">

    <div class="container content">
        <div class="row">
            <div class="col-md-12">
                <h1 style="color:#ed1c24; text-align: center">{!! $block->firstWhere('label', 'heading_5')->content !!}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <img class="bank-1" src="{{asset($logoBank->image)}}" style="text-align: center; width: 100%">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style=" text-align:center">
                {!! $block->firstWhere('label', 'desciption')->content !!}
            </div>
        </div>

</section>

<section style="padding-top: 1%;">
    <div class="container content">
        <div class="row">
            <div class="col-md-12">
                @php
                    $btn = unserialize($block->firstWhere('label', 'button')->content);
                @endphp
                <a href="{{$btn['link']}}" target="_blank"> <h4 style="color:#ffffff;background-color: #ed1c24; padding-top: 15px;padding-bottom: 15px; text-align: center">{{$btn['caption']}}</h4></a>
            </div>
        </div>
    </div>
</section>
@stop