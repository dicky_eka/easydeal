@extends('layouts.web')

@section('content')
<section style="padding-top:50px; padding-bottom:20px">
        <div class="container content" style="margin-bottom:0">
            <div class="row">
                <div class="col-md-4">
                    <div class="title-block" style="text-align: center;">
                        <h3 style="margin-bottom:15px; color:#ed1c24">BOOK YOUR UNIT NOW</h3>
                        <h6>Hanya agen kantor terdaftar yang bisa booking unit</h6>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="title-block" style="text-align: center;">
                        <h3 style="color:#ed1c24; padding:0; margin:0">HUBUNGI</h3>
                        <h2 style="color:#ed1c24; padding:0; margin:0">1500 238</h2>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="title-block" style="text-align: center;">
                        <a href="https://youtu.be/vPpZXYzmdEc"><h3 style="background-color:#ed1c24;color:#ffffff; padding:10px; text-align: center">WATCH TVC</h3></a>
                    </div>
                </div>

            </div>
        </div>
</section>
<section style="padding-top:0px">
    <div class="container content text-center">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="videoWrapper">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/vPpZXYzmdEc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></iframe>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</section>

<section style="padding-top:10px; padding-bottom:10%">
    <div class="container">
        <div class="intro">
            <h1 style="color:#ed1c24; text-align:center;">PROJECTS</h1>
        </div>


        <div class="row" style="padding-bottom: 2%">
            @foreach($projects as $project)
            <div class="col-md-4" style="padding-bottom: 4%">
                <a class="project-link" href="{{$project->url}}"><img src="{{asset($project->image)}}" width="100%"></a>
            </div>
            @endforeach
        </div>




    </div>
    </div>
</section>

@stop