@extends('layouts.web')

@section('content')
    <section style="padding-top:30px; padding-bottom: 0px; ">
        <div class="container content"><!-- 1 -->
            @foreach($brochures as $brochure=>$values)
            <!-- panel 1 -->
            <div class="row"> <!-- 2 -->
                <div class="col-md-12"><!-- 3 -->
                    <button class="accordion button-text">{{$brochure}}</button> <!-- button open and close -->
                    <div class="panel"><!-- 4 -->
                        <!-- panel 1 row 1 -->
                        <div class="row" ><!-- 5 -->
                            @foreach($values as $value)
                            <div class="col-lg-6" style="padding-top: 5%"><!-- 6 -->
                                <div class="content-brochure" style="background-color: #ed1c24; width:100%; padding: 0.5% 0.5% 0.5% 0;text-align: center"> <!-- 7 -->

                                    <div class="row" style="padding:0"><!-- 8 -->
                                        <div class="col-lg-3"><!-- 9 -->
                                            <img src="images/ebrochure/pdf.png" width="auto">
                                        </div><!-- 9 -->
                                        <div class="col-lg-9"><!-- 10 -->
                                            <h3 style="margin-top: 35px; color:#ffffff">{{$value->title}}</h3>
                                            <p style="color:white">{{$value->description}}</p>
                                        </div><!-- 10 -->
                                    </div><!-- 8 -->

                                    <div class="row" style="padding:0"><!-- 11 -->
                                        <div class="col-lg-12" style="text-align: right; padding-right: 10%"><!-- 12 -->
                                            <a href="{{asset($value->file)}}" target="_blank" class="download-button"><h5>Download</h5></a>
                                        </div><!-- 12 -->
                                    </div><!-- 11 -->

                                </div><!-- 7 -->

                            </div><!-- 6 -->
                            @endforeach


                        </div><!-- 5 --> <!-- panel 1 row 1 ends -->
                    </div><!-- 4 -->
                </div><!-- 3 -->
            </div><!-- 2 -->
            @endforeach




        </div> <!-- 1 -->
    </section><!-- panel 1 ends-->
@stop




@section('js')
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active1");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
</script>

@stop