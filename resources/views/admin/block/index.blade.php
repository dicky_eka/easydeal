@extends('adminlte::page')

@section('title', 'Block - All')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content_header')
    <h1>Data Block</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Block</h3>
                </div>
                <table class="table table-bordered">
                    <tr>
                        <th>Name</th>
                        <th>Label</th>
                        <th>Content</th>
                        <th style="width:200px;">Action</th>
                    </tr>
                    @foreach ($blocks as $block)
                        <tr>
                            <td>{!! $block->name !!}</td>
                            <td>{!! $block->label !!}</td>
                            <td>{!! $block->content !!}</td>
                            <td>
                                <a href="{!! route('admin.block.edit',$block->id)!!}"><button class="btn btn-primary"><i class="fa fa fa-edit"></i></button></a>
                                <a onclick="checkDelete({{$block->id}})"  class="btn btn-danger" >Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <div class="box-footer clearfix">
                    {!! $paginate !!}
                </div>
            </div>



        </div>
    </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.7/sweetalert2.all.min.js"></script>
    <script>
        function checkDelete(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                $.ajax({
                    url:" {{ url('admin/block/')}}/"+id,
                    type: 'DELETE',
                })
                .done(function(res) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                    location.reload();
                })
            }
        })
        }
    </script>
@stop

