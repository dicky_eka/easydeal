@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
@stop

@section('title', 'Block - Edit')

@section('content_header')
    <h1>Edit Block</h1>
@stop

@section('content')
    <div class="row">
        {!! Form::model($block,['route'=>['admin.block.update',$block->id],'method'=>'PATCH']) !!}
        {{  Form::hidden('url',URL::previous())  }}
        <div class="col-lg-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Block</h3>
                </div>
                <div class="box-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Ada inputan yang salah.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                        <div class="form-group">
                            <label for="">Name</label>
                            {!! Form::text('name',null, ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <label for="">Label</label>
                            {!! Form::text('label',null, ['class'=>'form-control']) !!}
                        </div>

                        @switch($block->type)
                            @case('string')
                                <div class="form-group">
                                    <label for="">Content</label>
                                    {!! Form::textarea('content',null,['class'=>'form-control ckeditor']) !!}
                                </div>
                            @break
                            @case('img')
                                <div class="form-group">
                                    <label for=""> Image</label>
                                    <div class="input-group">
                                  <span class="input-group-btn">
                                    <a id="lfm" data-input="image" data-preview="holder" class="btn btn-primary">
                                      <i class="fa fa-picture-o"></i> Choose
                                    </a>
                                  </span>
                                        {!! Form::text('content', null, ["class"=>"form-control","id"=>"image"]) !!}
                                    </div>
                                    <img id="holder" style="margin-top:15px;max-height:100px;">
                                </div>
                            @break
                            @case('btn')
                                @php
                                    $btn = unserialize($block->content);
                                @endphp
                                <div class="form-group">
                                    <label for="">Link</label>
                                    {!! Form::url("content[link]",$btn['link'], ['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="">Caption</label>
                                    {!! Form::text("content[caption]",$btn['caption'], ['class'=>'form-control']) !!}
                                </div>
                            @break
                        @endswitch
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{ URL::previous() }}" class="btn btn-warning" style="padding: 6px 20px;">Back</a>

                </div>
            </div>
        </div>

        {!! Form::close() !!}

    </div>


@stop

@section('js')
    <script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script src="https://cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
    <script>
        $('#lfm').filemanager('image');
        CKEDITOR.replace( 'ckeditor' );
    </script>

@stop
