@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
@stop

@section('title', 'Contact - Edit')

@section('content_header')
    <h1>Edit Contact</h1>
@stop

@section('content')
    <div class="row">
        {!! Form::model($contact,['route'=>['admin.contact.update',$contact->id],'method'=>'PATCH']) !!}

        <div class="col-lg-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Contact</h3>
                </div>
                <div class="box-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Ada inputan yang salah.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <div class="form-group">
                     <label for="">Logo Image</label>
                    <div class="input-group">
                      <span class="input-group-btn">
                        <a id="lfm" data-input="logo_img" data-preview="holder" class="btn btn-primary">
                          <i class="fa fa-picture-o"></i> Choose
                        </a>
                      </span>
                            {!! Form::text('logo_img', null, ["class"=>"form-control","id"=>"logo_img"]) !!}
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;">
                    </div>
                    <div class="form-group">
                        <label for="">Address</label>
                        {!! Form::text('address',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label for="">Phone</label>
                        {!! Form::text('phone',null, ['class'=>'form-control']) !!}
                    </div>       

                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{ URL::previous() }}" class="btn btn-warning" style="padding: 6px 20px;">Back</a>

                </div>
            </div>
        </div>

        {!! Form::close() !!}

    </div>


@stop

@section('js')
    <script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script>
        $('#lfm').filemanager('image');
    </script>

@stop
