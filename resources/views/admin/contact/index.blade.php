@extends('adminlte::page')

@section('title', 'Contact - All')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content_header')
    <h1>Data Contact</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-6 ">
            <div class="row">
                <div class="col-lg-9 ">
                    <form role="search">
                        <div class="input-group">
                            <input type="text" value="" name="q" class="form-control">
                            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">Cari Data</button>
              </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <br>
        <br>
        <br>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Contact</h3>
                </div>
                <table class="table table-bordered">
                    <tr>
                        <th>Logo Image</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th style="width:200px;">Action</th>
                    </tr>
                    @foreach ($contacts as $contact)
                        <tr>
                            <td> <img  width="150px"src="{{$contact->logo_img}}" class="img-responsive" alt="Image"></td>
                            <td>{{$contact->address}} </td>
                            <td>{{$contact->phone}}</td>                            
                            <td>
                                <a href="{!! route('admin.contact.edit',$contact->id)!!}"><button class="btn btn-primary"><i class="fa fa fa-edit"></i></button></a>
                                <a onclick="checkDelete({{$contact->id}})"  class="btn btn-danger" >Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <div class="box-footer clearfix">
                    {!! $paginate !!}
                </div>
            </div>



        </div>
    </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.7/sweetalert2.all.min.js"></script>
    <script>
        function checkDelete(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                $.ajax({
                    url:" {{ url('admin/contact/')}}/"+id,
                    type: 'DELETE',
                })
                .done(function(res) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                    location.reload();
                })
            }
        })
        }
    </script>
@stop

