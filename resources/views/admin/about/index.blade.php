@extends('adminlte::page')

@section('title', 'About - All')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content_header')
    <h1>Data About</h1>
@stop

@section('content')
    <div class="row">
        @if(session()->has('alert'))
            <div class="col-lg-12">                
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        &times;
                    </button>                    
                    {{ session()->get('alert') }}
                </div>                
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data About</h3>
                </div>
                <table class="table table-bordered">
                    <tr>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Instagram</th>
                        <th>Facebook</th>
                        <th>Twitter</th>
                        <th style="width:200px;">Action</th>
                    </tr>
                    @foreach ($abouts as $about)
                        <tr>
                            <td>{{$about->address}}</td>
                            <td>{{$about->phone}}</td>
                            <td>{{$about->email}}</td>
                            <td><a href="https://www.instagram.com/{{$about->ig}}">{{$about->ig}}</a></td>
                            <td><a href="http://facebook.com/{{$about->fb}}">{{$about->fb}}</a></td>
                            <td><a href="https://twitter.com/{{$about->tw}}">{{$about->tw}}</a></td>
                            <td>
                                <a href="{!! route('admin.about.edit',$about->id)!!}"><button class="btn btn-primary"><i class="fa fa fa-edit"></i></button></a>
                                <a onclick="checkDelete({{$about->id}})"  class="btn btn-danger" >Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <div class="box-footer clearfix">
                    {!! $paginate !!}
                </div>
            </div>



        </div>
    </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.7/sweetalert2.all.min.js"></script>
    <script>
        function checkDelete(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                $.ajax({
                    url:" {{ url('admin/about/')}}/"+id,
                    type: 'DELETE',
                })
                .done(function(res) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                    location.reload();
                })
            }
        })
        }
    </script>
@stop

