@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
@stop

@section('title', 'Expo - Edit')

@section('content_header')
    <h1>Edit Expo</h1>
@stop

@section('content')
    <div class="row">
        {!! Form::model($expo,['route'=>['admin.expo.update',$expo->id],'method'=>'PATCH']) !!}

        <div class="col-lg-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Expo</h3>
                </div>
                <div class="box-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Ada inputan yang salah.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <div class="form-group">
                        <label for="">On Date</label>
                        {!! Form::text('on_date',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label for="">Location</label>
                        {!! Form::text('location',null, ['class'=>'form-control']) !!}
                    </div>      

                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{ URL::previous() }}" class="btn btn-warning" style="padding: 6px 20px;">Back</a>

                </div>
            </div>
        </div>

        {!! Form::close() !!}

    </div>


@stop

@section('js')
@stop
