@extends('adminlte::page')

@section('title', 'Logos - All')

@section('css')
    <style>
        .img-row{
            margin-bottom: 20px;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content_header')
    <h1>Data Logos</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Logos</h3>
                </div>
                <div class="box-body">
                        <div class="row">
                        @foreach ($logos as $logo)
                            {{-- expr --}}
                            <div class="col-md-3 img-row">
                                <div class="thumbnail" >
                                    <div class="square" >
                                        <img src="{{$logo->image}}" class="img-responsive" alt="Image">
                                    </div>
                                </div>
                                <div class="caption text-center">
                                    <a  onclick="checkDelete({{$logo->id}})" class="btn btn-danger" >Delete
                                    </a>
                                    <a href="{{ route('admin.logo.edit', $logo->id) }}"  class="btn btn-default" >
                                        Edit
                                    </a>
                                </div>
                            </div>
                        @endforeach

                </div>
            </div>

        </div>
    </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.7/sweetalert2.all.min.js"></script>
    <script>
        function checkDelete(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                $.ajax({
                    url:" {{ url('admin/logo/')}}/"+id,
                    type: 'DELETE',
                })
                .done(function(res) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                    location.reload();
                })
            }
        })
        }
    </script>
@stop

