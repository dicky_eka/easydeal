@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
@stop

@section('title', 'Gift - Tambah')

@section('content_header')
    <h1>Add Gift</h1>
@stop

@section('content')
    <div class="row">
        {!! Form::open(['route'=>'admin.gift.store']) !!}

        <div class="col-lg-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Gift</h3>
                </div>
                <div class="box-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Ada inputan yang salah.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="">Title</label>
                        {!! Form::text('title',null, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <label for=""> Image</label>
                        <div class="input-group">
                          <span class="input-group-btn">
                            <a id="lfm" data-input="logo_img" data-preview="holder" class="btn btn-primary">
                              <i class="fa fa-picture-o"></i> Choose
                            </a>
                          </span>
                                {!! Form::text('image', null, ["class"=>"form-control","id"=>"logo_img"]) !!}
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;">
                    </div>


                    <div class="form-group">
                        <label for="">Content</label>
                        {!! Form::textarea('description',null,['class'=>'form-control editor1']) !!}
                    </div>


                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="{{ URL::previous() }}" class="btn btn-warning" style="padding: 6px 20px;">Back</a>

                </div>
            </div>
        </div>
        {!! Form::close() !!}

    </div>


@stop

@section('js')
  <script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    $('#lfm').filemanager('image');
    tinymce.init({ selector:'textarea' });
</script>

@stop

