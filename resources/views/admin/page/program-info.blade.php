@extends('adminlte::page')

@section('title', 'Page | Program Info')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content_header')
    <h1>Page Program Info</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="panel-title">Heading</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('admin.block.edit',$block->firstWhere('label', 'heading_1')->id)}}">
                            <button type="button" class="btn  btn-info btn-xs">Edit</button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                        {!! $block->firstWhere('label', 'heading_1')->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="panel-title">Banner Image</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('admin.block.edit',$block->firstWhere('label', 'banner')->id)}}">
                            <button type="button" class="btn  btn-info btn-xs">Edit</button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                        <img src="{{ asset($block->firstWhere('label', 'banner')->content) }}" class="img-responsive" alt="Image">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="panel-title">Heading</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('admin.block.edit',$block->firstWhere('label', 'heading_2')->id)}}">
                            <button type="button" class="btn  btn-info btn-xs">Edit</button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                        <a href="{{route('admin.block.edit',$block->firstWhere('label', 'heading_2')->id)}}">
                        {!! $block->firstWhere('label', 'heading_2')->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Gift</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('admin.gift.index')}}">
                            <button type="button" class="btn  btn-info btn-xs">Edit</button>
                        </a>
                    </div>
                </div>
                <table class="table table-bordered">
                    <tbody><tr>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Description</th>
                    </tr>
                    @foreach ($gifts as $gift)
                    <tr>
                        <td> <img  width="150px"src="{{$gift->image}}" class="img-responsive" alt="Image"></td>
                        <td>{!! $gift->title !!}</td>
                        <td>{!! $gift->description !!}</td>
                        <td>
                    </td>
                    @endforeach
                </table>
                <div class="box-footer clearfix">

                </div>
            </div>



        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="panel-title">Heading</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('admin.block.edit',$block->firstWhere('label', 'heading_3')->id)}}">
                            <button type="button" class="btn  btn-info btn-xs">Edit</button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                        {!! $block->firstWhere('label', 'heading_3')->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="panel-title">Projec Logo</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('admin.logo.index')}}">
                            <button type="button" class="btn  btn-info btn-xs">Edit</button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                        <div class="row">
                            @foreach($logos as $logo)
                            <div class="col-md-2 img-row">
                                <div class="thumbnail">
                                    <div class="square">
                                        <img src="{{asset($logo->image)}}" class="img-responsive" alt="Image">
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="panel-title">Heading</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('admin.block.edit',$block->firstWhere('label', 'heading_4')->id)}}">
                            <button type="button" class="btn  btn-info btn-xs">Edit</button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                        {!! $block->firstWhere('label', 'heading_4')->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="panel-title">Heading</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('admin.block.edit',$block->firstWhere('label', 'heading_5')->id)}}">
                            <button type="button" class="btn  btn-info btn-xs">Edit</button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                        {!! $block->firstWhere('label', 'heading_5')->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="panel-title">Heading</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('admin.block.edit',$block->firstWhere('label', 'desciption')->id)}}">
                            <button type="button" class="btn  btn-info btn-xs">Edit</button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                        {!! $block->firstWhere('label', 'desciption')->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="panel-title">Button</h3>
                    <div class="box-tools pull-right">
                        <a  href="{{route('admin.block.edit',$block->firstWhere('label', 'button')->id)}}">
                            <button type="button" class="btn  btn-info btn-xs">Edit</button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                        @php
                        $btn = unserialize($block->firstWhere('label', 'button')->content);
                        @endphp

                        <a href="{{$btn['link']}}">
                            <button type="button" class="btn btn-default">{{$btn['caption']}}</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>





@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.7/sweetalert2.all.min.js"></script>
    <script>
        function checkDelete(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                $.ajax({
                    url:" {{ url('admin/location/')}}/"+id,
                    type: 'DELETE',
                })
                    .done(function(res) {
                        swal(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                        location.reload();
                    })
            }
        })
        }
    </script>
@stop

