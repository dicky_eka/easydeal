@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
@stop

@section('title', 'Post - Edit')

@section('content_header')
    <h1>Edit News</h1>
@stop

@section('content')
    <div class="row">
        {!! Form::model($post,['route'=>['admin.post.update',$post->id],'method'=>'PATCH']) !!}

        <div class="col-lg-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Post</h3>
                </div>
                <div class="box-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Ada inputan yang salah.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="">Title</label>
                        {!! Form::text('title',null, ['class'=>'form-control']) !!}
                    </div>



                    <div class="form-group">
                        <label for="">Content</label>
                        {!! Form::textarea('content',null,['class'=>'form-control editor1']) !!}
                    </div>

                    <div class="form-group">
                        <label for="">Tags</label>
                        {!! Form::text('tags',null, ['class'=>'form-control']) !!}
                    </div>


                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{ URL::previous() }}" class="btn btn-warning" style="padding: 6px 20px;">Back</a>

                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Publish Date</h3>
                </div>
                <div class="box-body">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!!
                                  Form::date('published_at',$post->published_at->format('Y-m-d'), ["class"=>"form-control pull-right","id"=>"datepicker"]) !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Image Cover</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <div class="input-group">
                      <span class="input-group-btn">
                        <a id="lfm" data-input="image" data-preview="holder" class="btn btn-primary">
                          <i class="fa fa-picture-o"></i> Choose
                        </a>
                      </span>
                            {!! Form::text('image', null, ["class"=>"form-control","id"=>"image"]) !!}
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;">
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">SEO </h3>
                </div>
                <div class="box-body">
                    <label for="">Keyword</label>
                    <div class="form-group">
                        {!! Form::text('meta_keyword',null, ['class'=>'form-control']) !!}
                    </div>
                    <label for="">Description</label>
                    <div class="form-group">
                        {!! Form::textarea('meta_description', null, ['class'=>'form-control','rows'=>3]) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}

    </div>


@stop

@section('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script>
        $('#lfm').filemanager('image');
    </script>
    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea.editor1",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>
@stop
