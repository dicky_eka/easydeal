<ul class="nav">
    <li><a href="{{url('/')}}">HOME </a></li>
    <li><a href="program-info.html" style="color:#ed1c24;">PROGRAM INFO</a></li>
    <li><a href="{{url('project')}}">PROJECTS</a></li>
    <li><a href="#">AGENTS</a>
        <ul class="dropdown">
            <li><a href="http://respro.sinarmasland.com/registeragent.aspx" target="_blank">Agent Registration</a></li>
            <li><a href="{{url('agent')}}">Assigned Agent List</a></li>
        </ul>
    </li>
    <li><a href="{{url('brochure')}}">E-BROCHURE</a></li>
    <li><a href="{{url('expo')}}">EXPO & EVENT  </a></li>
    <li><a href="{{url('post')}}">NEWS </a></li>
    <li><a href="{{url('contact')}}">CONTACT </a></li>
</ul>
<div class="copyright">2018 &copy; Sinar Mas Land. All rights reserved.</div>
<div class="exit"></div>
<ul class="social">
    <li><a href="https://twitter.com/{{$about->tw}}" class="fa fa-twitter" target="_blank"></a></li>
    <li><a href="http://facebook.com/sinarmasland/{{$about->fb}}" class="fa fa-facebook" target="_blank"></a></li>
    <li><a href="https://www.instagram.com/{{$about->ig}}" class="fa fa-instagram" target="_blank"></a></li>
</ul>