<!DOCTYPE html>
<html>
<head>
    <title>Easy Deal by Sinar Mas Land</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/base.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom.css')}}">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <script src="{{asset('assets/js/assets/jquery.js')}}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116866429-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-116866429-1');
    </script>

    @yield('meta')

</head>
<body>
<div class="body-overlay"></div>
<!-- Menu-->
<div class="nav-container">
    @include('layouts.sidebar')
</div>
<!-- Wrapper-->
<div id="wrapper" class="container-fluid smooth-transition">
    <!-- Header-->
    <header>
        <div class="container">
            <!-- Branding--><a href="index.html" class="brand"> <img alt="" src="images/logo.svg" class="logo"></a>
            <!-- Menu-->
            <div class="menu"></div>
        </div>
    </header>
    <section style="padding-top: 0; padding-bottom:50px">
        <!-- Background-->
        <img src="{{asset('images/bg-main.png')}}" style="width:100%;padding:0;margin:0"></section>
    </section>

    @yield('content')

    <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-angle-up"></i></button>
    <!-- Footer-->
    <footer>
        <div class="container content">
            <div class="row">
                <!-- Contact info-->
                <div class="col-md-4">
                    <p>Sinar Mas Land<br>
                    {{$about->address}}
                    </p>
                </div>
                <div class="col-md-3">
                    <p>P: 1500 238<br>M: {{$about->email}}</p>
                </div>
                <div class="col-md-5 text-right">
                    <!-- Social links-->
                    <ul class="social">
                        <li><a href="https://twitter.com/{{$about->tw}}" class="fa fa-twitter" target="_blank"></a></li>
                        <li><a href="http://facebook.com/{{$about->fb}}" class="fa fa-facebook" target="_blank"></a></li>
                        <li><a href="https://www.instagram.com/{{$about->ig}}" class="fa fa-instagram" target="_blank"></a></li>
                    </ul>
                    <!-- Copyright message-->
                    <div class="copyright">{{date("Y")}} &copy; Sinar Mas Land. All rights reserved.</div>

                </div>

            </div>
        </div>
    </footer>
    <section style="padding:0;margin:0; background-color: #f0f0f0"><img src="{{asset('images/footer.svg')}}" style="width:100%;padding:0;margin:0"></section>
</div>

<script>
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
</script>
<script src="{{asset('assets/js/assets/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/assets/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('assets/js/assets/validation.js')}}"></script>
<script src="{{asset('assets/js/assets/masonry.pkgd.min.js')}}"></script>
<script src="{{asset('assets/modules/tera-slider/tera-slider.js')}}"></script>
<script src="{{asset('assets/modules/tera-lightbox/tera-lightbox.js')}}"></script>
<script src="{{asset('assets/js/assets/animsition.js')}}"></script>
<script src="{{asset('assets/js/functions.js')}}"></script>
@yield('js')
</body>
</html>