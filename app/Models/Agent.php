<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected  $guarded = ['id'];

    public function location(){
        return $this->belongsTo(AgentLocation::class);
    }
}
