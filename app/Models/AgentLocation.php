<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentLocation extends Model
{

    protected  $guarded = ['id'];

    public function agents()
    {
        return $this->hasMany(Agent::class,'location_id');
    }


}
