<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\AgentLocation;
use App\Models\Block;
use App\Models\Brochure;
use App\Models\Contact;
use App\Models\Expo;
use App\Models\Gift;
use App\Models\Logo;
use App\models\Project;
use Illuminate\Http\Request;

class PageController extends Controller
{
    protected  $project;
    protected  $agentLocation;
    protected $expo;
    protected $contact;

    function __construct()
    {
        $this->project = new Project();
        $this->agentLocation = new AgentLocation();
        $this->expo = new Expo();
        $this->contact = new Contact();
    }

    public function home()
    {
        $projects = $this->project->take(15)->get();
        return view('web.home',compact('projects'));

    }

    public function project()
    {
        $projects = $this->project->get();
        return view('web.project',compact('projects'));
    }

    public function agent()
    {
        $agentLocation = $this->agentLocation->with('agents')->get();
        return view('web.agent-list',compact('agentLocation'));
    }

    public function expo()
    {
        $expos = $this->expo->get();
        return view('web.expo',compact('expos'));
    }

    public function contact()
    {
        $contacts = $this->contact->get();

        return view('web.contact',compact('contacts'));
    }


    public function brochure(Brochure $brochure){
        $brochures = $brochure->get()->groupBy('status');


        return view('web.brochure',compact('brochures'));
    }

    public function programInfo()
    {
        $gift = Gift::all();
        $logoProject = Logo::where('type','project')->get();
        $logoBank = Logo::where('type','bank')->first();
        $block = Block::get();

        return view('web.program-info',compact('gift','logoProject','logoBank','block'));
    }

}
