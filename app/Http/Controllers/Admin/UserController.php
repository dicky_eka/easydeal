<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Observers\UserObserver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    protected  $user;    

    function __construct()
    {
        $this->user = new User();        
    }


    public function index(Request $request)
    {
        $users = $this->user                  
                    ->where('name','LIKE','%'.$request->q.'%')
                    ->paginate(10);
        $paginate = $users->appends($request->except('page'))->links();

        return view('admin.user.index',compact('users','paginate'));

    }

    public function create()
    {     
        return view('admin.user.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
           	'name' 					=> 'required',
           	'email'					=> 'required|email|unique:users',
            'password'				=> 'required|min:6|confirmed',
			'password_confirmation' => 'required|min:6'                   
        ]);

        $this->user->create($request->all());

        return redirect()->route('admin.user.index');
    }

    public function edit($id)
    {       
        $user =  $this->user->find($id);
        return view('admin.user.edit',compact('user'));
    }

    public function update($id,Request $request)
    {
        $user =  $this->user->find($id);

        $this->validate($request,[
           	'name' 		=> 'required',
           	'email'		=> 'required|email',
            'password'	=> 'min:6',
        ]);

        $data = [
            'name' => $request->name,
            'email' => $request->email
        ];


        if ($request->has('password')){
            $data['password'] = bcrypt($request->password);
        }

        $user->update($data);

        return redirect()->route('admin.user.index');

    }

    public function destroy($id)
    {
        $this->user->find($id)->delete();
        return url('admin/user');
    }

}