<?php

namespace App\Http\Controllers\Admin;

use App\Models\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AboutController extends Controller
{
    protected  $about;    

    function __construct()
    {
        $this->about = new About();        
    }


    public function index(Request $request)
    {
        $abouts = $this->about                  
                    ->where('address','LIKE','%'.$request->q.'%')
                    ->paginate(10);
        $paginate = $abouts->appends($request->except('page'))->links();

        return view('admin.about.index',compact('abouts','paginate'));

    }

    public function create()
    {      
    	$data = DB::table('abouts')->exists();

    	if ($data == null) {
    		return view('admin.about.create');
		}else{			
			session()->flash('alert','Data already exist!');
			return redirect()->route('admin.about.index');
		}  	
    	
    }

    public function store(Request $request)
    {    	
		$this->validate($request,[
           	'address' 	=> 'required',
            'phone'		=> 'required',
            'email'		=> 'required',
            'ig'		=> 'required',
            'fb'		=> 'required',
            'tw'		=> 'required',                       
    	]);

        $this->about->create($request->all());

        return redirect()->route('admin.about.index');			
    }

    public function edit($id)
    {       
        $about =  $this->about->find($id);
        return view('admin.about.edit',compact('about'));
    }

    public function update($id,Request $request)
    {
        $about =  $this->about->find($id);
        $this->validate($request,[
           	'address' 	=> 'required',
            'phone'		=> 'required',
            'email'		=> 'required',
            'ig'		=> 'required',
            'fb'		=> 'required',
            'tw'		=> 'required',                       
        ]);

        $about->update($request->all());

        return redirect()->route('admin.about.index');

    }

    public function destroy($id)
    {
        $this->about->find($id)->delete();
        return url('admin/about');
    }

}