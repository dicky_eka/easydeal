<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    protected  $contact;    

    function __construct()
    {
        $this->contact = new Contact();        
    }


    public function index(Request $request)
    {
        $contacts = $this->contact                  
                    ->where('address','LIKE','%'.$request->q.'%')
                    ->paginate(10);
        $paginate = $contacts->appends($request->except('page'))->links();

        return view('admin.contact.index',compact('contacts','paginate'));

    }

    public function create()
    {     
        return view('admin.contact.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
           	'logo_img' 	=> 'required',
            'address'	=> 'required',
            'phone' 	=> 'required|numeric',            
        ]);

        $this->contact->create($request->all());

        return redirect()->route('admin.contact.index');
    }

    public function edit($id)
    {       
        $contact =  $this->contact->find($id);
        return view('admin.contact.edit',compact('contact'));
    }

    public function update($id,Request $request)
    {
        $contact =  $this->contact->find($id);
        $this->validate($request,[
           	'logo_img' 	=> 'required',
            'address'	=> 'required',
            'phone' 	=> 'required|numeric',           
        ]);

        $contact->update($request->all());

        return redirect()->route('admin.contact.index');

    }

    public function destroy($id)
    {
        $this->contact->find($id)->delete();
        return url('admin/contact');
    }
}