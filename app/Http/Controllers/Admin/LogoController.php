<?php

namespace App\Http\Controllers\Admin;

use App\Models\Logo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogoController extends Controller
{
    protected  $logo;

    function __construct()
    {
        $this->logo = new Logo();
    }


    public function index(Request $request)
    {
        $logos = $this->logo->paginate(10);
        $paginate = $logos->appends($request->except('page'))->links();

        return view('admin.logo.index',compact('logos','paginate'));

    }

    public function create()
    {
        $type =[
            'project' => 'Project',
            'bank' =>'Bank'
        ];

        return view('admin.logo.create',compact('type'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'image' 	=> 'required',
            'type'	    => 'required',
        ]);

        $this->logo->create($request->all());

        return redirect()->route('admin.logo.index');
    }

    public function edit($id)
    {
        $type =[
            'bank' =>'Bank',
            'project' => 'Project'
        ];

        $logo =  $this->logo->find($id);
        return view('admin.logo.edit',compact('logo','type'));
    }

    public function update($id,Request $request)
    {
        $logo =  $this->logo->find($id);
        $this->validate($request,[
            'image' 	=> 'required',
            'type'	    => 'required',
        ]);

        $logo->update($request->all());

        return redirect()->route('admin.logo.index');
    }

    public function destroy($id)
    {
        $this->logo->find($id)->delete();
        return url('admin/logo');
    }
}
