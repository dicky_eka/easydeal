<?php

namespace App\Http\Controllers\Admin;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    protected  $project;    

    function __construct()
    {
        $this->project = new Project();        
    }


    public function index(Request $request)
    {
        $projects = $this->project                  
                    ->where('name','LIKE','%'.$request->q.'%')
                    ->paginate(10);
        $paginate = $projects->appends($request->except('page'))->links();

        return view('admin.project.index',compact('projects','paginate'));

    }

    public function create()
    {     
        return view('admin.project.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
           	'name' 	=> 'required',
            'url' 	=> 'required',
            'image' => 'required',            
        ]);

        $this->project->create($request->all());

        return redirect()->route('admin.project.index');
    }

    public function edit($id)
    {       
        $project =  $this->project->find($id);
        return view('admin.project.edit',compact('project'));
    }

    public function update($id,Request $request)
    {
        $project =  $this->project->find($id);
        $this->validate($request,[
           	'name' => 'required',
            'url' => 'required',
            'image' => 'required',            
        ]);

        $project->update($request->all());

        return redirect()->route('admin.project.index');

    }

    public function destroy($id)
    {
        $this->project->find($id)->delete();
        return url('admin/project');
    }
}