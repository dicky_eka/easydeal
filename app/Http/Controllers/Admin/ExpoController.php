<?php

namespace App\Http\Controllers\Admin;

use App\Models\Expo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExpoController extends Controller
{
    protected  $expo;    

    function __construct()
    {
        $this->expo = new Expo();        
    }


    public function index(Request $request)
    {
        $expos = $this->expo                  
                    ->where('location','LIKE','%'.$request->q.'%')
                    ->paginate(10);
        $paginate = $expos->appends($request->except('page'))->links();

        return view('admin.expo.index',compact('expos','paginate'));

    }

    public function create()
    {     
        return view('admin.expo.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
           	'on_date' 	=> 'required',
            'location'	=> 'required',                       
        ]);

        $this->expo->create($request->all());

        return redirect()->route('admin.expo.index');
    }

    public function edit($id)
    {       
        $expo =  $this->expo->find($id);
        return view('admin.expo.edit',compact('expo'));
    }

    public function update($id,Request $request)
    {
        $expo =  $this->expo->find($id);
        $this->validate($request,[
           	'on_date' 	=> 'required',
            'location'	=> 'required',                       
        ]);

        $expo->update($request->all());

        return redirect()->route('admin.expo.index');

    }

    public function destroy($id)
    {
        $this->expo->find($id)->delete();
        return url('admin/expo');
    }
}