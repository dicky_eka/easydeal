<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brochure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrochureController extends Controller
{
    protected  $brochure;    

    function __construct()
    {
        $this->brochure = new Brochure();        
    }


    public function index(Request $request)
    {
        $brochures = $this->brochure                  
                    ->where('title','LIKE','%'.$request->q.'%')
                    ->paginate(10);
        $paginate = $brochures->appends($request->except('page'))->links();

        return view('admin.brochure.index',compact('brochures','paginate'));

    }

    public function create()
    {
        $status = [
            'READY STOCK' => 'READY STOCK',
            'UNDER CONSTRUCTION' => 'UNDER CONSTRUCTION',
            'INDENT' => 'INDENT'
        ];

        return view('admin.brochure.create',compact('status'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
           	'status' 		=> 'required',
            'title'			=> 'required',
            'file'			=> 'required',
            'description'	=> 'required',
        ]);

        $this->brochure->create($request->all());

        return redirect()->route('admin.brochure.index');
    }

    public function edit($id)
    {
        $status = [
            'READY STOCK' => 'READY STOCK',
            'UNDER CONSTRUCTION' => 'UNDER CONSTRUCTION',
            'INDENT' => 'INDENT'
        ];

        $brochure =  $this->brochure->find($id);
        return view('admin.brochure.edit',compact('brochure','status'));
    }

    public function update($id,Request $request)
    {
        $brochure =  $this->brochure->find($id);
        $this->validate($request,[
           	'status' 		=> 'required',
            'title'			=> 'required',
            'file'			=> 'required',
            'description'	=> 'required',
        ]);

        $brochure->update($request->all());

        return redirect()->route('admin.brochure.index');

    }

    public function destroy($id)
    {
        $this->brochure->find($id)->delete();
        return url('admin/brochure');
    }


}