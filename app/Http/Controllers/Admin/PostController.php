<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    protected $post;

    function __construct()
    {
        $this->post = new Post();
    }

    public function index(Request $request)
    {
        $posts = $this->post
            ->where('title','LIKE','%'.$request->q.'%')
            ->paginate(10);
        $paginate = $posts->appends($request->except('page'))->links();

        return view('admin.post.index',compact('posts','paginate'));

    }

    public function create()
    {
        return view('admin.post.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' =>'required',
            'content' => 'required',
            'published_at' => 'required'
        ]);

        $extends = [
            'slug'=>str_slug($request->title),
            'user_id'=>auth()->user()->id
        ];

        $this->post->create($request->all()+$extends);

        return redirect()->route('admin.post.index');
    }


    public function edit($id)
    {
        $post =  $this->post->find($id);
        return view('admin.post.edit',compact('post'));
    }

    public function update($id,Request $request)
    {
        $post =  $this->post->find($id);

        $this->validate($request,[
            'title' =>'required',
            'content' => 'required',
            'published_at' => 'required'
        ]);


        $post->update($request->all()+['slug'=>str_slug($request->title)]);

        return redirect()->route('admin.post.index');
    }

    public function destroy($id)
    {
        $this->post->find($id)->delete();
        return url('admin/post');
    }

}
