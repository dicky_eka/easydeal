<?php

namespace App\Http\Controllers\Admin;

use App\Models\Agentlocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgentLocationController extends Controller
{
    protected  $location;    

    function __construct()
    {
        $this->location = new Agentlocation();        
    }


    public function index(Request $request)
    {
        $locations = $this->location                  
                    ->where('name','LIKE','%'.$request->q.'%')
                    ->paginate(10);
        $paginate = $locations->appends($request->except('page'))->links();

        return view('admin.location.index',compact('locations','paginate'));

    }

    public function create()
    {     
        return view('admin.location.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
           	'name' 	=> 'required',                      
        ]);

        $this->location->create($request->all());

        return redirect()->route('admin.location.index');
    }

    public function edit($id)
    {       
        $location =  $this->location->find($id);
        return view('admin.location.edit',compact('location'));
    }

    public function update($id,Request $request)
    {
        $location =  $this->location->find($id);
        $this->validate($request,[
           	'name' 	=> 'required',                      
        ]);

        $location->update($request->all());

        return redirect()->route('admin.location.index');

    }

    public function destroy($id)
    {
        $this->location->find($id)->delete();
        return url('admin/agentl');
    }


}