<?php

namespace App\Http\Controllers\Admin;

use App\Models\Block;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlockController extends Controller
{
    protected  $block;

    function __construct()
    {
        $this->block = new Block();
    }


    public function index(Request $request)
    {
        $blocks = $this->block->paginate(10);
        $paginate = $blocks->appends($request->except('page'))->links();

        return view('admin.block.index',compact('blocks','paginate'));

    }

    public function create()
    {
       return abort(404);
    }

    public function store(Request $request)
    {
        return abort(404);
    }

    public function edit($id)
    {
        $block =  $this->block->find($id);
        return view('admin.block.edit',compact('block'));
    }

    public function update($id,Request $request)
    {
        $block =  $this->block->find($id);

        $this->validate($request,[
            'name' => 'required',
            'label' => 'required',
        ]);

        if($block->type == 'btn') {
            $content = serialize($request->content);
        }else{
            $content = $request->content;
        }

        $block->update([
            'name' => $request->name,
            'label' => $request->label,
            'content' => $content
        ]);

        return redirect($request->input('url'));


    }

    public function destroy($id)
    {
        $this->block->find($id)->delete();
        return url('admin/block');
    }
}
