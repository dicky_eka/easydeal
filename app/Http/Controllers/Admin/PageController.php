<?php

namespace App\Http\Controllers\Admin;

use App\Models\Block;
use App\Models\Gift;
use App\Models\Logo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function programInfo()
    {
        $block = Block::get();
        $gifts = Gift::get();
        $logos = Logo::where('type','project')->get();

        return view('admin.page.program-info',compact('block','gifts','logos'));

    }
}
