<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gift;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GiftController extends Controller
{
    protected  $gift;

    function __construct()
    {
        $this->gift = new Gift();
    }


    public function index(Request $request)
    {
        $gifts = $this->gift
            ->where('title','LIKE','%'.$request->q.'%')
            ->paginate(10);
        $paginate = $gifts->appends($request->except('page'))->links();

        return view('admin.gift.index',compact('gifts','paginate'));

    }

    public function create()
    {
        return view('admin.gift.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'image' => 'required',
        ]);

        $this->gift->create($request->all());

        return redirect()->route('admin.gift.index');
    }

    public function edit($id)
    {
        $gift =  $this->gift->find($id);
        return view('admin.gift.edit',compact('gift'));
    }

    public function update($id,Request $request)
    {
        $gift =  $this->gift->find($id);
        $this->validate($request,[
            'title' => 'required',
            'image' => 'required',
        ]);

        $gift->update($request->all());

        return redirect()->route('admin.gift.index');

    }

    public function destroy($id)
    {
        $this->gift->find($id)->delete();
        return url('admin/gift');
    }
}
