<?php

namespace App\Http\Controllers\Admin;

use App\Models\Agent;
use App\Models\AgentLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgentController extends Controller
{
    protected  $agent;
    protected  $location;

    function __construct()
    {
        $this->agent = new Agent();
        $this->location = new AgentLocation();
    }


    public function index(Request $request)
    {
        $agents = $this->agent
                    ->with('location')
                    ->where('name','LIKE','%'.$request->q.'%')
                    ->paginate(10);
        $paginate = $agents->appends($request->except('page'))->links();

        return view('admin.agent.index',compact('agents','paginate'));

    }

    public function create()
    {
        $locations = $this->location->pluck('name','id');
        return view('admin.agent.create',compact('locations'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
           'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'address' => 'required'
        ]);

        $this->agent->create($request->all());

        return redirect()->route('admin.agent.index');
    }

    public function edit($id)
    {
        $locations = $this->location->pluck('name','id');
        $agent =  $this->agent->find($id);
        return view('admin.agent.edit',compact('agent','locations'));
    }

    public function update($id,Request $request)
    {
        $agent =  $this->agent->find($id);
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'address' => 'required'
        ]);

        $agent->update($request->all());

        return redirect()->route('admin.agent.index');

    }

    public function destroy($id)
    {
        $this->agent->find($id)->delete();
        return url('admin/agentl');
    }

}
