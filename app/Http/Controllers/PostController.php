<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $post;

    function __construct()
    {
        $this->post = new Post();
    }

    public function index(Request $request)
    {
        $posts = $this->post->all();


        return view('web.post.index',compact('posts'));
    }

    public function show($slug)
    {
        $post =  $this->post->where('slug',$slug)->first();

        return view('web.post.show',compact('post'));
    }



}
