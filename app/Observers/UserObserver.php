<?php

namespace App\Observers;

use App\User;
use Illuminate\Support\Facades\Hash;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        $user->update([
            'password' => bcrypt($user->password)
        ]);
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleting(User $user)
    {
        //
    }
}